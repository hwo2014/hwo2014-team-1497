import csv
import sys
import collections

def readValues(name):
    values = collections.defaultdict(list)
    with open(name, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            for (k, v) in row.items():
                values[k].append(float(v))
    return values

if __name__ == '__main__':
    values = readValues(sys.argv[1])
