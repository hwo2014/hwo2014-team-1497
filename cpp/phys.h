#ifndef HWO_PHYS_H
#define HWO_PHYS_H

#include <cmath>
#include <functional>

namespace phys
{
  struct World
  {
    double pos;
    double angle;
    double velocity;
    double angularVelocity;
    double force;
    double trackResistance;
    double mass;
    double length;
    double width;
    double flagpos;
    double radius;
    double pieceAngle;
  };

  World &evalWorld(World &world, double time);

  inline double rectInertia(double mass, double height, double width)
  {
    return mass*(height*height + width*width)/12.0;
  }
}

#endif
