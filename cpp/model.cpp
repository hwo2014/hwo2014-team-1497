#include "model.h"

#include <limits>

// TODO: where can we find this?
namespace model
{
  double trackResistance()
  {
    return 1.0;
  }

  double maxCentrifugalForce()
  {
    return 19.0;
  }

  double carMass()
  {
    return 50.0;
  }

  double maxForce()
  {
    return 10.0;
  }

  double maxAngle()
  {
    return 60.0;
  }

  double forceToThrottle(double force)
  {
    const double FORCE_MAX = maxForce();
    if(force < FORCE_MAX)
      return force/FORCE_MAX;
    else
      return 1.0;
  }
}
