#ifndef HWO_MODEL_H
#define HWO_MODEL_H

namespace hwo_protocol
{
  struct Car;
}

namespace model
{
  // TODO: constexpr
  double trackResistance();
  double carMass();
  double carIntertia();
  double forceToThrottle(double force);
  double maxForce();
  double maxCentrifugalForce();
}

#endif
