#include <algorithm>
#include <cmath>
#include <iostream>
#include <iterator>
#include <sstream>
#include <fstream>
#include <unordered_map>
#include <vector>
#include <string>
#include <utility>
#include <stdexcept>

#include "phys.h"
#include "model.h"
#include "trig.h"
#include "tracer.h"

namespace
{
  typedef std::unordered_map<std::string, double> Record;
  typedef std::vector<Record> RecordCollection;
}

namespace
{
  // TODO: move
  double angleToRadian(double angle)
  {
    return angle/180.0*M_PI;
  }
  double radianToAngle(double radian)
  {
    return radian/M_PI*180.0;
  }

  double toDouble(const std::string &str)
  {
    std::stringstream stream(str);
    double res = 0.0;
    stream>>res;
    return res;
  }

  std::vector<std::string> split(const std::string &str, char delim)
  {
    std::vector<std::string> result;
    std::string::size_type pos = 0;
    std::string::size_type prevPos = 0;
    while((pos = str.find(delim, prevPos)) != std::string::npos)
    {
      result.push_back(str.substr(prevPos, pos - prevPos));
      prevPos = pos+1;
    }
    result.push_back(str.substr(prevPos));
    return result;
  }

  RecordCollection readData(const std::string &filename)
  {
    RecordCollection data;
    std::ifstream fin(filename);
    if(!fin.is_open())
      return data;
    if(fin)
    {
      typedef std::vector<double> ValueCollection;
      std::string header;
      fin>>header;
      const auto fields = split(header, ',');
      while(fin)
      {
        std::string line;
        fin>>line;
        const auto strs = split(line, ',');
        ValueCollection values;
        std::transform(strs.begin(), strs.end(), std::back_inserter(values),
          toDouble);
        Record record;
        for(auto it = fields.begin(); it != fields.end(); ++it)
        {
          if(static_cast<std::size_t>(it - fields.begin()) < values.size())
            record[*it] = values[it - fields.begin()];
          else
            record[*it] = Record::mapped_type();
        }
        data.push_back(record);
      }
    }
    return data;
  }

  phys::World &inputData(phys::World &world, const Record &record)
  {
    world.force = record.at("force");
    world.radius = record.at("radius");
    world.pieceAngle = record.at("piece_angle");
    return world;
  }

  Record outputData(const phys::World &world)
  {
    Record record;
    record["test_velocity"] = world.velocity;
    record["test_angular_velocity"] = radianToAngle(world.angularVelocity);
    record["test_angle"] = radianToAngle(world.angle);
    return record;
  }
}

int main(int argc, char *argv[])
{
  if(argc != 2)
  {
    std::cerr<<"usage: "<<argv[0]<<" <filename>"<<std::endl;
    return 1;
  }
  const auto data = readData(argv[1]);
  Tracer tracer("test.csv", {"test_velocity", "test_angular_velocity",
    "test_angle"});
  phys::World world;
  world.pos = 0.0;
  world.angle = 0.0;
  world.velocity = 0.0;
  world.angularVelocity = 0.0;
  world.force = 0.0;
  world.trackResistance = model::trackResistance();
  world.mass = model::carMass();
  world.length = 40.0;
  world.width = 20.0;
  world.flagpos = 10.0;
  world.radius = 0.0;
  world.pieceAngle = 0.0;
  std::cout<<"testing"<<std::endl;
  {
    const auto record = outputData(world);
    for(const auto &p : record)
      tracer.set(p.first, p.second);
    tracer.commit();
  }
  for(const auto &r : data)
  {
    try
    {
      inputData(world, r);
      phys::evalWorld(world, 1.0);
      {
        const auto record = outputData(world);
        for(const auto &p : record)
          tracer.set(p.first, p.second);
        tracer.commit();
      }
    }
    catch(const std::out_of_range&)
    {
      std::cerr<<"invalid record"<<std::endl;
      break;
    }
  }
  std::cout<<"done"<<std::endl;
  return 0;
}
