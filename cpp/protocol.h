#ifndef HWO_PROTOCOL_H
#define HWO_PROTOCOL_H

#include <string>
#include <iostream>
#include <jsoncons/json.hpp>

namespace hwo_protocol
{
  class ProtocolException: public std::exception
  {
  public:
    virtual ~ProtocolException() throw()
    {}

    virtual const char *what() const throw()
    {
      return "protocol exception";
    }
  };

  struct Id
  {
    std::string name;
    std::string color;
  };
  inline bool operator==(const Id &left, const Id &right)
  {
      return left.name == right.name && left.color == right.color;
  }

  struct Piece
  {
    double length;
    double radius;
    double angle;
    bool swtch;
  };
  typedef std::vector<Piece> PieceCollection;

  struct LanePosition
  {
    int startLaneIndex;
    int endLaneIndex;
  };

  struct PiecePosition
  {
    int pieceIndex;
    double inPieceDistance;
    int lap;
    LanePosition lane;
  };

  struct CarDimensions
  {
      double length;
      double width;
      double guideFlagPosition;
  };

  struct Lane
  {
      int distanceFromCenter;
      int index;
  };
  typedef std::vector<Lane> LaneCollection;

  struct Car
  {
      Id id;
      CarDimensions dimensions;
  };
  typedef std::vector<Car> CarCollection;

  struct RaceSession
  {
      int laps;
      int maxLapTimeMs;
      bool quickRace;
  };

  struct Track
  {
      std::string id;
      std::string name;
      PieceCollection pieces;
      LaneCollection lanes;
  };

  struct Race
  {
      Track track;
      CarCollection cars;
      RaceSession raceSession;
  };

  struct CarPosition
  {
    Id id;
    double angle;
    PiecePosition piecePosition;
  };
  typedef std::vector<CarPosition> CarPositionCollection;

  struct CarPositions {
    CarPositionCollection data;
  };

  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data);
  jsoncons::json make_join(const std::string& name, const std::string& key);
  jsoncons::json make_ping();
  jsoncons::json make_throttle(double throttle);

  CarPositions makeCarPositions(const jsoncons::json &data);
  Id makeMyCar(const jsoncons::json &data);
  Race makeRace(const jsoncons::json &data);
}

#endif
