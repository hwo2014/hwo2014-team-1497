#include "tracer.h"

#include <ostream>

namespace
{
  template<class Collection>
  std::ostream &writeCollection(std::ostream &stream, const Collection &collection)
  {
    bool first = true;
    for(const auto &v : collection)
    {
      if(!first)
        stream<<',';
      else
        first = false;
      stream<<v;
    }
    return stream;
  }
}

Tracer::Tracer(const std::string &name,
  std::initializer_list<std::string> fields)
  :index(), values(fields.size(), 0.0), fout(name)
{
  for(auto it = fields.begin(); it != fields.end(); ++it)
    index[*it] = it - fields.begin();
  writeCollection(fout, fields)<<std::endl;
}

void Tracer::set(const std::string &field, double value)
{
  auto it = index.find(field);
  if(it != index.end())
  {
    values[it->second] = value;
  }
}

void Tracer::commit()
{
  writeCollection(fout, values)<<std::endl;
  for(auto &v : values)
    v = 0.0;
}
