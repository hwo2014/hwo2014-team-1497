#ifndef HWO_TRIG_H
#define HWO_TRIG_H

namespace trig
{
  double triangleAngle(double adjacent, double opposite, double angle);
  double triangleSide(double adjacent, double opposite, double angle);
}

#endif
