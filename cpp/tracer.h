#ifndef HWO_TRACER_H
#define HWO_TRACER_H

#include <cstddef>
#include <fstream>
#include <string>
#include <unordered_map>
#include <vector>

class Tracer
{
public:
  Tracer(const std::string &name, std::initializer_list<std::string> fields);

  void set(const std::string &field, double value);
  void commit();

private:
  typedef std::unordered_map<std::string, std::size_t> IndexMap;
  typedef std::vector<double> ValueCollection;

private:
  IndexMap index;
  ValueCollection values;
  std::ofstream fout;
};

#endif
