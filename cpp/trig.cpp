#include "trig.h"

#include <cmath>

namespace trig
{
  double triangleAngle(double adjacent, double opposite, double angle)
  {
    return atan(1.0/(adjacent/(opposite*sin(angle)) - 1.0/tan(angle)));
  }

  double triangleSide(double adjacent, double opposite, double angle)
  {
    return std::sqrt(adjacent*adjacent + opposite*opposite -
      2.0*adjacent*opposite*std::cos(angle));
  }
}
