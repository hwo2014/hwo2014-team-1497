#include "protocol.h"

#include <exception>
#include <string>
#include <vector>

namespace hwo_protocol
{
  namespace
  {
    template<typename T>
    void readField(T &field, const jsoncons::json &data)
    {
      field = data.as<T>();
    }

    template<typename T>
    void readOptionalField(T &field, const jsoncons::json &data,
      const char *name)
    {
      if(data.has_member(name))
        readField(field, data[name]);
      else
        field = T();
    }
  }

  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data)
  {
    jsoncons::json r;
    r["msgType"] = msg_type;
    r["data"] = data;
    return r;
  }

  jsoncons::json make_join(const std::string& name, const std::string& key)
  {
    jsoncons::json data;
    data["name"] = name;
    data["key"] = key;
    return make_request("join", data);
  }

  jsoncons::json make_ping()
  {
    return make_request("ping", jsoncons::null_type());
  }

  jsoncons::json make_throttle(double throttle)
  {
    return make_request("throttle", throttle);
  }

  Id make_id(const jsoncons::json &data) {
    try
    {
      Id result;
      result.name = data["name"].as<std::string>();
      result.color = data["color"].as<std::string>();
      return result;
    }
    catch(const jsoncons::json_exception&)
    {
      throw ProtocolException();
    }
  }

  LanePosition makeLanePosition(const jsoncons::json &data)
  {
    try
    {
      LanePosition lane;
      lane.startLaneIndex = data["startLaneIndex"].as<int>();
      lane.endLaneIndex = data["endLaneIndex"].as<int>();
      return lane;
    }
    catch(const jsoncons::json&)
    {
      throw ProtocolException();
    }
  }

  PiecePosition makePiecePosition(const jsoncons::json &data)
  {
    try
    {
      PiecePosition result;
      result.pieceIndex = data["pieceIndex"].as<int>();
      result.inPieceDistance = data["inPieceDistance"].as<double>();
      result.lap = data["lap"].as<int>();
      result.lane = makeLanePosition(data["lane"]);
      return result;
    }
    catch(const jsoncons::json_exception&)
    {
      throw ProtocolException();
    }
  }

  Piece makePiece(const jsoncons::json &data)
  {
    try
    {
      Piece piece;
      readOptionalField(piece.length, data, "length");
      readOptionalField(piece.radius, data, "radius");
      readOptionalField(piece.angle, data, "angle");
      readOptionalField(piece.swtch, data, "switch");
      return piece;
    }
    catch(const jsoncons::json_exception&)
    {
      throw ProtocolException();
    }
  }

  CarPosition make_car_position(const jsoncons::json &data)
  {
    try
    {
      CarPosition result;
      result.id = make_id(data["id"]);
      result.angle = data["angle"].as<double>();
      result.piecePosition = makePiecePosition(data["piecePosition"]);
      return result;
    }
    catch(const jsoncons::json_exception&)
    {
      throw ProtocolException();
    }
  }

  CarPositions makeCarPositions(const jsoncons::json &data)
  {
    try
    {
      CarPositions result;
      for(auto it = data.begin_elements();
        it != data.end_elements(); ++it)
        result.data.push_back(make_car_position(*it));
      return result;
    }
    catch(const jsoncons::json_exception&)
    {
      throw ProtocolException();
    }
  }

  Id makeMyCar(const jsoncons::json &data)
  {
    try
    {
      return make_id(data);
    }
    catch(const jsoncons::json_exception&)
    {
      throw ProtocolException();
    }
  }

  Lane makeLane(const jsoncons::json &data)
  {
    try
    {
      Lane lane;
      readField(lane.distanceFromCenter, data["distanceFromCenter"]);
      readField(lane.index, data["index"]);
      return lane;
    }
    catch(const jsoncons::json_exception&)
    {
      throw ProtocolException();
    }
  }

  CarDimensions makeCarDimensions(const jsoncons::json &data)
  {
    try
    {
      CarDimensions carDimensions;
      readField(carDimensions.length, data["length"]);
      readField(carDimensions.width, data["width"]);
      readField(carDimensions.guideFlagPosition, data["guideFlagPosition"]);
      return carDimensions;
    }
    catch(const jsoncons::json_exception&)
    {
      throw ProtocolException();
    }
  }

  RaceSession makeRaceSession(const jsoncons::json &data)
  {
    try
    {
      RaceSession raceSession;
      readField(raceSession.laps, data["laps"]);
      readField(raceSession.maxLapTimeMs, data["maxLapTimeMs"]);
      readField(raceSession.quickRace, data["quickRace"]);
      return raceSession;
    }
    catch(const jsoncons::json_exception&)
    {
      throw ProtocolException();
    }
  }

  Track makeTrack(const jsoncons::json &data)
  {
    try
    {
      Track track;
      readField(track.id, data["id"]);
      readField(track.name, data["name"]);
      for(auto it = data["pieces"].begin_elements();
        it != data["pieces"].end_elements(); ++it)
        track.pieces.push_back(makePiece(*it));
      for(auto it = data["lanes"].begin_elements();
        it != data["lanes"].end_elements(); ++it)
        track.lanes.push_back(makeLane(*it));
      return track;
    }
    catch(const jsoncons::json_exception&)
    {
      throw ProtocolException();
    }
  }

  Car makeCar(const jsoncons::json &data)
  {
    try
    {
      Car car;
      car.id = make_id(data["id"]);
      car.dimensions = makeCarDimensions(data["dimensions"]);
      return car;
    }
    catch(const jsoncons::json_exception&)
    {
      throw ProtocolException();
    }
  }

  Race makeRace(const jsoncons::json &data)
  {
    try
    {
      Race race;
      race.track = makeTrack(data["race"]["track"]);
      for(auto it = data["race"]["cars"].begin_elements();
        it != data["race"]["cars"].end_elements(); ++it)
        race.cars.push_back(makeCar(*it));
      race.raceSession = makeRaceSession(data["race"]["raceSession"]);
      return race;
    }
    catch(const jsoncons::json_exception&)
    {
      throw ProtocolException();
    }
  }

}  // namespace hwo_protocol
