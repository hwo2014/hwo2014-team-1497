#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <utility>
#include <iostream>
#include <jsoncons/json.hpp>

#include "protocol.h"
#include "tracer.h"

class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  msg_vector react(const jsoncons::json& msg);

private:
  struct Turn
  {
    double distance;
    double radius;
    double length;
    int sign;
  };

private:
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
  const std::map<std::string, action_fun> action_map;

  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);
  msg_vector on_your_car(const jsoncons::json& data);
  msg_vector on_game_init(const jsoncons::json& data);

  double calcThrottle(const hwo_protocol::CarPosition &carPosition);
  Turn nextTurn(const hwo_protocol::CarPosition &carPosition) const;

  double pieceLength(const hwo_protocol::Piece &piece,
    const hwo_protocol::CarPosition &carPosition) const;

  double distance(const hwo_protocol::CarPosition &begin,
    const hwo_protocol::CarPosition &end) const;

  void updateVelocity(const hwo_protocol::CarPosition &currentPos);

private:
  hwo_protocol::Id myId;
  hwo_protocol::Car myCar;
  hwo_protocol::Race race;

  double velocity = 0.0;
  double angularVelocity = 0.0;
  double force = 0.0;

  hwo_protocol::CarPosition prevPos;

  Tracer tracer;
};

#endif
