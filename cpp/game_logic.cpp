#include "game_logic.h"

#include <algorithm>
#include <cmath>
#include <cassert>
#include <cstddef>
#include <limits>

#include "protocol.h"
#include "phys.h"
#include "model.h"

using namespace hwo_protocol;

namespace
{
  double calcRadiusThrottle(double radius)
  {
    if(radius > 0.0)
    {
      const double THR = 50.0;
      if(radius > THR)
        return 1.0 - (0.9 * (1.0/(radius/THR)));
      else
        return 0.1;
    }
    else
    {
      return 1.0;
    }
  }

  double calcPieceThrottle(const hwo_protocol::Piece &piece)
  {
    return calcRadiusThrottle(piece.radius);
  }

  double angleToRadian(double angle)
  {
    return angle/180.0*M_PI;
  }

  double radianToAngle(double radian)
  {
    return radian/M_PI*180.0;
  }
}

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "yourCar", &game_logic::on_your_car },
      { "gameInit", &game_logic::on_game_init }
    },
    tracer("hwo.csv", {"pos", "velocity", "angular_velocity", "angle", "radius", "piece_angle", "force", "throttle",
      "exit_angle", "lane_velocity", "turn_radius", "turn_velocity"})
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  try
  {
    const auto carPositions = makeCarPositions(data);
    for(const auto &pos : carPositions.data)
    {
      if(pos.id == myId)
      {
        updateVelocity(pos);
        double throttle = calcThrottle(pos);
        tracer.set("velocity", velocity);
        tracer.set("angular_velocity", angularVelocity);
        tracer.set("throttle", throttle);
        tracer.set("pos", pos.piecePosition.inPieceDistance);
        tracer.commit();
        return { make_throttle(throttle) };
      }
    }
    std::cerr<<"car id not found"<<std::endl;
    return { make_throttle(0.5) };
  }
  catch(const ProtocolException&)
  {
    std::cerr<<"protocol exception on 'CARPOSITIONS'"<<std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
  try
  {
    myId = makeMyCar(data);
  }
  catch(const ProtocolException&)
  {
    std::cerr<<"protocol exception on 'YOURCAR'"<<std::endl;
    return { make_ping() };
  }
  std::cout<<"my car: '"<<myId.name<<"' '"<<myId.color<<'\''<<std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
  std::cout << "game init" << std::endl;
  try
  {
    race = makeRace(data);
    CarPosition current;
    current.id = myId;
    current.angle = 0;
    current.piecePosition.pieceIndex = 0;
    current.piecePosition.inPieceDistance = 0;
    current.piecePosition.lap = 0;
    prevPos = current;
    std::cout<<"pieces: "<<race.track.pieces.size()<<std::endl;
    bool carFound = false;
    for(auto it = race.cars.begin(); it != race.cars.end(); ++it)
    {
      if(it->id == myId)
      {
        myCar = *it;
        std::cout<<"my car: length="<<myCar.dimensions.length
          <<" width="<<myCar.dimensions.width
          <<" flagpos="<<myCar.dimensions.guideFlagPosition
          <<std::endl;
        carFound = true;
      }
    }
    if(!carFound)
      std::cerr<<"my car description not found"<<std::endl;
    force = 0.1;
  }
  catch(const ProtocolException&)
  {
    std::cerr<<"protocol exception on 'GAMEINIT'"<<std::endl;
  }
  return { make_ping() };
}

double game_logic::calcThrottle(const hwo_protocol::CarPosition &carPosition)
{
  if(carPosition.piecePosition.pieceIndex >= 
    race.track.pieces.size())
  {
    std::cerr<<"invalid piece index: "<<carPosition.piecePosition.pieceIndex
      <<std::endl;
    return 0.5;
  }
  const auto turn = nextTurn(carPosition);
  const auto &currentPiece =
    race.track.pieces[carPosition.piecePosition.pieceIndex];
  const double turnRadius = turn.radius - race.track.lanes[
    carPosition.piecePosition.lane.startLaneIndex].distanceFromCenter;
  const double radius = (currentPiece.radius != 0.0)
    ?currentPiece.radius - race.track.lanes[
    carPosition.piecePosition.lane.startLaneIndex].distanceFromCenter:0.0;
  tracer.set("turn_radius", turnRadius);
  tracer.set("radius", radius);
  tracer.set("piece_angle", currentPiece.angle);
  phys::World world;
  world.pos = 0.0;
  world.angle = angleToRadian(carPosition.angle);
  world.velocity = velocity;
  world.angularVelocity = angleToRadian(angularVelocity);
  world.force = force;
  world.trackResistance = model::trackResistance();
  world.mass = model::carMass();
  world.length = myCar.dimensions.length;
  world.width = myCar.dimensions.width;
  world.flagpos = myCar.dimensions.guideFlagPosition;
  world.radius = radius;
  world.pieceAngle = currentPiece.angle;
  const double maxTurnVelocity = std::sqrt(turnRadius * model::maxCentrifugalForce()/model::carMass());
  tracer.set("turn_velocity", maxTurnVelocity);
  if(turn.distance > 0.0)
  {
    world.force = model::maxForce();
    phys::evalWorld(world, 1.0);
    world.force = 0.0;
    while(world.pos < turn.distance && world.velocity > maxTurnVelocity)
    {
      phys::evalWorld(world, 1.0);
    }
    if(world.velocity < maxTurnVelocity)
      force = model::maxForce();
    else
      force = 0.0;
  }
  else
  {
    const phys::World oldWorld = world;
    world.force = force;
    phys::evalWorld(world, 1.0);
    if(world.velocity > maxTurnVelocity)
    {
      do
      {
        force -= 0.1;
        world = oldWorld;
        world.force = force;
        phys::evalWorld(world, 1.0);
      }
      while(force > 0.0 && world.velocity > maxTurnVelocity);
    }
    else
    {
      do
      {
        force += 0.1;
        world = oldWorld;
        world.force = force;
        phys::evalWorld(world, 1.0);
      }
      while(force < model::maxForce() && world.velocity < maxTurnVelocity);
      force -= 0.1;
    }
  }
  if(force < 0.0)
    force = 0.0;
  tracer.set("angle", carPosition.angle);
  tracer.set("force", force);
  double newThrottle = model::forceToThrottle(force);
  return newThrottle;
}

game_logic::Turn game_logic::nextTurn(
  const hwo_protocol::CarPosition &carPosition) const
{
  Turn turn;
  turn.distance = 0.0;
  turn.length = 0.0;
  turn.radius = 0.0;
  double turning = false;
  for(std::size_t i = 0; i < race.track.pieces.size(); ++i)
  {
    const auto &piece = race.track.pieces[
      (carPosition.piecePosition.pieceIndex + i)%race.track.pieces.size()];
    // TODO: fix for different signs
    if(piece.radius > 0.0)
    {
      if(turning)
      {
        turn.radius = std::min(piece.radius, turn.radius);
      }
      else
      {
        turning = true;
        turn.sign = piece.angle>0?1:-1;
        turn.radius = piece.radius;
      }
      turn.length += pieceLength(piece, carPosition);
      if(i == 0)
        turn.length -= carPosition.piecePosition.inPieceDistance;
    }
    else
    {
      if(turning)
      {
        break;
      }
      else
      {
        turn.distance += pieceLength(piece, carPosition);
        if(i == 0)
          turn.distance -= carPosition.piecePosition.inPieceDistance;
      }
    }
  }
  return turn;
}

double game_logic::pieceLength(const hwo_protocol::Piece &piece,
  const hwo_protocol::CarPosition &carPosition) const
{
  if(piece.length > 0)
    return piece.length;
  return (piece.radius -
    race.track.lanes[
    carPosition.piecePosition.lane.startLaneIndex].distanceFromCenter)
    *(std::abs(piece.angle)/180.0*M_PI);
}

double game_logic::distance(const hwo_protocol::CarPosition &begin,
  const hwo_protocol::CarPosition &end) const
{
  double dist = end.piecePosition.inPieceDistance;
  if(begin.piecePosition.pieceIndex != end.piecePosition.pieceIndex)
  {
    size_t i = 1;
    while(i < race.track.pieces.size() && 
      (begin.piecePosition.pieceIndex + i)%race.track.pieces.size() !=
      end.piecePosition.pieceIndex)
    {
      dist += pieceLength(race.track.pieces[
        (begin.piecePosition.pieceIndex + i)%race.track.pieces.size()], end);
      ++i;
    }
    dist += pieceLength(race.track.pieces[begin.piecePosition.pieceIndex],
      begin) - begin.piecePosition.inPieceDistance;
  }
  else
  {
    dist -= begin.piecePosition.inPieceDistance;
  }
  return dist;
}

void game_logic::updateVelocity(const hwo_protocol::CarPosition &currentPos)
{
  // with the assumption one call per tick and ticks are uniform
  velocity = distance(prevPos, currentPos);
  angularVelocity = currentPos.angle - prevPos.angle;
  prevPos = currentPos;
}
