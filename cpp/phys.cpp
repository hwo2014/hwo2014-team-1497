#include "phys.h"

#include <cmath>
#include <limits>
#include <stdexcept>

#include "trig.h"

namespace phys
{
  World &evalWorld(World &world, double time)
  {
    const double DT = 0.001;
    const double INERTIA = rectInertia(world.mass, world.length, world.width);
    const double DIST = world.length/2.0 - world.flagpos;
    double t = 0.0;
    while(t < time)
    {
      const double resForce = world.force - world.velocity*world.trackResistance;
      const double accel = resForce/world.mass;
      double angularAccel = std::copysign(1.0,-world.pieceAngle)*(DIST*world.force*sin(world.angle)/INERTIA);
      if(world.radius != 0.0)
      {
        const double massRadius = trig::triangleSide(DIST, world.radius,
          M_PI/2.0+std::abs(world.angle));
        const double massVelocity = world.velocity * world.radius / massRadius;
        angularAccel -= std::copysign(DIST*world.mass*massVelocity*massVelocity
          /massRadius*sin(trig::triangleAngle(DIST, world.radius,
              M_PI/2.0+std::abs(world.angle)))
          /(INERTIA + world.mass*DIST*DIST), -world.pieceAngle);
      }
      world.velocity += accel * DT;
      world.angularVelocity += angularAccel * DT;
      world.pos += world.velocity * DT;
      world.angle += world.angularVelocity * DT;
      t += DT;
    }
    return world;
  }
}
